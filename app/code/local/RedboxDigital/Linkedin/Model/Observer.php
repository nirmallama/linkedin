<?php
class RedboxDigital_Linkedin_Model_Observer 
{

    public function configurelinkedinoption($observer)
    {
    	$required_value = Mage::getStoreConfig('managelinkedin/linkedinoption/isrequired', Mage::app()->getStore());


    	$write = Mage::getSingleton('core/resource')->getConnection('core_write');

		$write->query("
		  UPDATE eav_attribute 
		  SET  is_required = ".$required_value."
		  WHERE  attribute_code = 'linkedin_profile'
		  ");
		

         
    }
}